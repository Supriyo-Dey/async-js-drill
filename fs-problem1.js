/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/


const fs = require('fs')


function createDirectory(name,arr,  createFile, deleteFile){
    fs.mkdir(`./${name}`, (error) => {
        if(error){
            console.log("ERROR(DIR) : ");
            console.log(error);
        } else {
            createFile(arr,name,deleteFile);
        }
    });
}

function createFile(arr,name,callback){
    let count = 0;
    arr.forEach((element,index) => {
        fs.writeFile(`./${name}/${element}`,"Hello World", (error) => {
            if(error){
                console.log("ERROR(FILE) : ");
                console.log(error);
            } else {
                count++;
                console.log(`file-${index} created Successfully`);
                if(count == arr.length){
                    console.log(`All files created successfully`)
                    callback(name , arr);
                }
            }
        });
    })
}

function deleteFile(name , arr){
    arr.forEach((element)=>{
        fs.unlink(`./${name}/${element}`,(error) => {
            if(error){
                console.log("ERROR(DEL) : ");
                console.log(error);
            }else {
                console.log("DELETED!")
            }
        });

    })
}

function fsProblem1(directory,arr){
    createDirectory(directory, arr, createFile,deleteFile);
}

module.exports = fsProblem1;