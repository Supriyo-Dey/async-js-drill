/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/


const fs = require('fs');

function fsProblem2(path){
    fs.readFile(`${path}`, 'utf8', (error, data) => {
        if (error) {
            console.error('Error : ', error);
            return;
        }
    
        const upper = data.toUpperCase();
        fs.writeFile('lipsum_upper.txt', upper, (error) => {
            if (error) {
                console.error('Error :', error);
                return;
            }
    
            fs.readFile('lipsum_upper.txt', 'utf8', (error, data) => {
                if (error) {
                    console.error('Error :', error);
                    return;
                }
    
                const lowercasedContent = data.toLowerCase();
                const sentences = lowercasedContent.split('.');
                const sentenceContent = sentences.join('\n');
                fs.writeFile('sentences.txt', sentenceContent, (error) => {
                    if (error) {
                        console.error('Error :', error);
                        return;
                    }
    
                    fs.readFile('lipsum_upper.txt', 'utf8', (error, data) => {
                        if (error) {
                            console.error('Error :', error);
                            return;
                        }
    
                        const sortedContent = data.split('').sort().join('');
                        fs.writeFile('sorted.txt', sortedContent, (error) => {
                            if (error) {
                                console.error('Error :', error);
                                return;
                            }
    
                            const filenames = ['lipsum_upper.txt', 'sentences.txt', 'sorted.txt'];
                            fs.writeFile('filenames.txt', filenames.join('\n'), (error) => {
                                if (error) {
                                    console.error('Error :', error);
                                    return;
                                }
    
                                fs.readFile('filenames.txt', 'utf8', (error, data) => {
                                    if (error) {
                                        console.error('Error :', error);
                                        return;
                                    }
    
                                    const filesToDelete = data.split('\n');
                                    deleteFiles(filesToDelete);
                                });
                            });
                        });
                    });
                });
            });
        });
    });
}

function deleteFiles(filenames) {
    let count = 0;
    filenames.forEach((filename) => {
        fs.unlink(filename, (err) => {
            if (err) {
                console.error(`Error : `, err);
                return;
            }

            count++;
            if (count === filenames.length) {
                console.log('All files deleted');
            }
        });
    });
}

module.exports = fsProblem2;